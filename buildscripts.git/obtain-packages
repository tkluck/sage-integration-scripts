#!/usr/bin/env python

SAGE_SERVER="http://www.sagemath.org"

import tempfile
import contextlib
import shutil
import imp
import os
import sys

BUILDSCRIPTSDIR=os.path.dirname(os.path.abspath(sys.argv[0]))
SAGE_ROOT = os.getcwd()
SPKG_ROOT = os.path.join(SAGE_ROOT, 'spkg')

sageupdate = imp.load_source('sageupdate', os.path.join(BUILDSCRIPTSDIR, 'sage-update'))

local_packages = [ { 'name': 'sage',
                     'path': os.path.join(SAGE_ROOT, 'devel', 'sage-main'),
                     'extra_files': [os.path.join(BUILDSCRIPTSDIR, '.hg')],
                     'spkg': None
                   },
                   { 'name': 'sage_root',
                     'path': os.path.join(BUILDSCRIPTSDIR, 'empty_spkg'),
                     'extra_files': '',
                     'spkg': None
                   },
                   { 'name': 'sage_scripts',
                     'path': os.path.join(SAGE_ROOT, 'devel', 'scripts-main'), 
                     'extra_files': [os.path.join(BUILDSCRIPTSDIR, 'spkg-install')],
                     'spkg': None
                   },
                   { 'name': 'extcode',
                     'path': os.path.join(SAGE_ROOT, 'devel', 'ext-main'),
                     'extra_files': [],
                     'spkg': None
                   }
                ]
local_packages_by_name = { pkg['name']: pkg for pkg in local_packages }

#from plumbum.cmd import tar, rm, cp
# a poor man's version of plumbum, for portability:
import subprocess
def tar(*args):
    return subprocess.check_call(['tar'] + list(args))
def rm(*args):
    return subprocess.check_call(['rm'] + list(args))
def cp(*args):
    return subprocess.check_call(['cp'] + list(args))

@contextlib.contextmanager
def temp_pwd():
    """A context manager for creating and then deleting a temporary directory."""
    tmpdir = tempfile.mkdtemp()
    cwd = os.getcwd()
    try:
        os.chdir(tmpdir)
        yield 
    finally:
        os.chdir(cwd)
        shutil.rmtree(tmpdir)

def make_local_spkg(directory, spkg_name, extra_files):
    spkg_directory = os.path.join(SAGE_ROOT, 'spkg', 'standard', spkg_name)
    spkg_target_file = os.path.join(SAGE_ROOT, 'spkg', 'standard', spkg_name + ".spkg") 
    rm('-rf', spkg_directory)
    cp('-r', directory, spkg_directory)
    for filename in extra_files:
        cp('-r', filename, spkg_directory)
    rm('-f', spkg_target_file)
    tar('-cf', spkg_target_file, '--directory=%s' % os.path.dirname(spkg_directory),
        spkg_name)
    rm('-rf', spkg_directory)

if __name__ == "__main__":
    # fake the environment for sageupdate
    os.environ["SAGE_SERVER"] = SAGE_SERVER
    setattr(sageupdate, 'SAGE_ROOT', SAGE_ROOT)
    setattr(sageupdate, 'SPKG_ROOT', SPKG_ROOT)

    PKG_SERVER = sageupdate.get_sage_mirror()
    if not PKG_SERVER.endswith('/spkg'):
        PKG_SERVER += '/spkg'
    sageupdate.PKG_SERVER = PKG_SERVER

    with temp_pwd():
        available_spkg = sageupdate.spkg_list('standard')
    available_packages = []
    for spkg in available_spkg:
        name, version = spkg.split('-',1)
        version = sageupdate.chop(version, '.spkg')
        available_packages.append((name,  version, spkg))

    to_download = []
    to_make_locally = []
    for name, version, spkg in available_packages:
        if name in local_packages_by_name.keys():
            pkg = local_packages_by_name[name]
            pkg['spkg'] = spkg
            to_make_locally.append(pkg)
        else:
          to_download.append((name, spkg))

    for p in to_make_locally:
        make_local_spkg(
              os.path.join(SAGE_ROOT, p['path']),
              sageupdate.chop(p['spkg'], '.spkg'),
              p['extra_files'])

    os.chdir(os.path.join(SPKG_ROOT, "standard"))
    for name, spkg in to_download:
        # Note: don't use os.path.join when constructing arguments for
        # download_file: see the docstring for download_file.
        sageupdate.download_file("standard/%s" % spkg)

    os.chdir(SAGE_ROOT)
    sageupdate.download_file("standard/VERSION.txt")
